package camt.se234.lab11.service;

import camt.se234.lab11.dao.StudentDao;
import camt.se234.lab11.dao.StudentDaoImpl;
import camt.se234.lab11.entity.Student;
import org.hamcrest.number.IsCloseTo;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;

public class StudentServiceImplTest {
    StudentDao studentDao;
    StudentServiceImpl studentService;



    @Before
    public void setup(){
        studentDao = mock(StudentDao.class);
        studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
    }

    @Test
    public void testByIdFirst(){
        StudentDaoImpl studentDao = new StudentDaoImpl();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        assertThat(studentService.findStudentById("001"),
                is(new Student("001","Jon","Snow",2.33)));
        assertThat(studentService.findStudentById("002"),
                is(new Student("002","Arya","Stark",3.89)));
        assertThat(studentService.findStudentById("003"),
                is(new Student("003","Jaime","Lannister",2.88)));
        assertThat(studentService.findStudentById("004"),
                is(new Student("004","Daenerys","Targaryen",1.46)));
    }

    @Test
    public void testFindById(){
        List<Student> mockStudents= new ArrayList<>();
        mockStudents.add(new Student("001","Jon","Snow",2.33));
        mockStudents.add(new Student("002","Arya","Stark",3.89));
        mockStudents.add(new Student("003","Jaime","Lannister",2.88));
        mockStudents.add(new Student("004","Daenerys","Targaryen",1.46));
        mockStudents.add(new Student("005","Jaqen","H'ghar",0.45));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentById("001"), is((new Student("001","Jon","Snow",2.33))));
        assertThat(studentService.findStudentById("002"), is((new Student("002","Arya","Stark",3.89))));
        assertThat(studentService.findStudentById("003"), is((new Student("003","Jaime","Lannister",2.88))));
        assertThat(studentService.findStudentById("004"), is((new Student("004","Daenerys","Targaryen",1.46))));
    }
    @Test
    public void testGetAverageGPA(){
        List<Student> mockStudents= new ArrayList<>();
        mockStudents.add(new Student("001","Jon","Snow",2.33));
        mockStudents.add(new Student("002","Arya","Stark",3.89));
        mockStudents.add(new Student("003","Jaime","Lannister",2.88));
        mockStudents.add(new Student("004","Daenerys","Targaryen",1.46));
        mockStudents.add(new Student("005","Jaqen","H'ghar",0.45));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.getAverageGpa(), is((2.33+3.89+2.88+1.46+.45)/5));

    }

    @Test
    public void testGetAverageGPASet2(){
        List<Student> mockStudents= new ArrayList<>();
        mockStudents.add(new Student("001","Han","Solo",2.39));
        mockStudents.add(new Student("002","Darth","Vader",3.50));
        mockStudents.add(new Student("003","Luke","Skywalker",1.33));
        mockStudents.add(new Student("004","Obiwan","Kenobi",0.88));
        mockStudents.add(new Student("005","R2","D2",4.00));
        when(studentDao.findAll()).thenReturn(mockStudents);
        studentService.setStudentDao(studentDao);
        assertThat(studentService.getAverageGpa(), is((2.39+3.50+1.33+0.88+4.00)/5));

    }

    @Test
    public void testWithMock(){
        List<Student> mockStudents= new ArrayList<>();
        mockStudents.add(new Student("123","A","temp",2.33));
        mockStudents.add(new Student("124","B","temp",2.33));
        mockStudents.add(new Student("223","C","temp",2.33));
        mockStudents.add(new Student("224","D","temp",2.33));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentById("123"),is(new
                Student("123","A","temp",2.33)));
    }

    @Test
    public void testWithMock2(){
        List<Student> mockStudents= new ArrayList<>();
        mockStudents.add(new Student("001","Harry","Potter",1.55));
        mockStudents.add(new Student("002","Rubeus","Hagrid",2.47));
        mockStudents.add(new Student("003","Albus","Dumbledore",4.00));
        mockStudents.add(new Student("004","Draco","Malfoy",0.00));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentById("003"),is(new
                Student("003","Albus","Dumbledore",4.00)));
    }

    @Test
    public void testArverageGradeWithMock1(){
        List<Student> mockStudents= new ArrayList<>();
        mockStudents.add(new Student("001","Harry","Potter",1.55));
        mockStudents.add(new Student("002","Rubeus","Hagrid",2.47));
        mockStudents.add(new Student("003","Albus","Dumbledore",4.00));
        mockStudents.add(new Student("004","Draco","Malfoy",0.00));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.getAverageGpa(),is((1.55+2.47+4.00+0.00)/4));
    }

    @Test
    public void testArverageGradeWithMock2(){
        List<Student> mockStudents= new ArrayList<>();
        mockStudents.add(new Student("001","Han","Solo",2.39));
        mockStudents.add(new Student("002","Darth","Vader",3.50));
        mockStudents.add(new Student("003","Luke","Skywalker",1.33));
        mockStudents.add(new Student("004","Obiwan","Kenobi",0.88));
        mockStudents.add(new Student("005","R2","D2",4.00));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.getAverageGpa(),is((2.39+3.50+1.33+0.88+4.00)/5));
    }

    @Test
    public void testArverageGradeWithMock3(){
        List<Student> mockStudents= new ArrayList<>();
        mockStudents.add(new Student("001","Jon","Snow",2.33));
        mockStudents.add(new Student("002","Arya","Stark",3.89));
        mockStudents.add(new Student("003","Jaime","Lannister",2.88));
        mockStudents.add(new Student("004","Daenerys","Targaryen",1.46));
        mockStudents.add(new Student("005","Jaqen","H'ghar",0.45));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.getAverageGpa(),is((2.33+3.89+2.88+1.46+.45)/5));
    }

    @Test
    public void testFindByPartOfId(){
        List<Student> mockStudents = new ArrayList<>();
        mockStudents.add(new Student("123","A","temp",2.33));
        mockStudents.add(new Student("124","B","temp",2.33));
        mockStudents.add(new Student("223","C","temp",2.33));
        mockStudents.add(new Student("224","D","temp",2.33));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentByPartOfId("22"), hasItem(new Student("223","C","temp",2.33)));
        assertThat(studentService.findStudentByPartOfId("22"),
                hasItems(new Student("223","C","temp",2.33)
                        ,new Student("224","D","temp",2.33)
                ));
    }

    @Test
    public void testFindByPartOfId2(){
        List<Student> mockStudents = new ArrayList<>();
        mockStudents.add(new Student("001","Jon","Snow",2.33));
        mockStudents.add(new Student("002","Arya","Stark",3.89));
        mockStudents.add(new Student("003","Jaime","Lannister",2.88));
        mockStudents.add(new Student("004","Daenerys","Targaryen",1.46));
        mockStudents.add(new Student("005","Jaqen","H'ghar",0.45));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentByPartOfId("00"), hasItem(new Student("001","Jon","Snow",2.33)));
        assertThat(studentService.findStudentByPartOfId("00"),
                hasItems(new Student("003","Jaime","Lannister",2.88)
                        ,new Student("004","Daenerys","Targaryen",1.46)
                        ,new Student("005","Jaqen","H'ghar",0.45)
                ));
    }

    @Test (expected = StudentServiceImpl.NoDataException.class)
    public void testNoDataException(){
        List<Student> mockStudents = new ArrayList<>();
        mockStudents.add(new Student("123","A","temp",2.33));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentById("55"),nullValue());
    }

    @Test (expected = StudentServiceImpl.DivideByZeroException.class)
    public void testNoDataException2(){
        List<Student> mockStudents = new ArrayList<>();
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.getAverageGpa(),nullValue());
    }
}
